<?php
Yii::setAlias('@engine', dirname(__DIR__));
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@modules', dirname(dirname(__DIR__)) . '/modules');
Yii::setAlias('@templates', dirname(dirname(__DIR__)) . '/templates');
Yii::setAlias('@cache', dirname(dirname(__DIR__)) . '/cache');
