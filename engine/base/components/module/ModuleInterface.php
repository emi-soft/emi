<?php
/**
 * Created by PhpStorm.
 * User: clanfavorite
 * Date: 4/30/2019
 * Time: 11:24 AM
 */

namespace engine\base\components\module;
use engine\base\Application;

/**
 * Interface ModuleInterface
 * @package ngine\base\interfaces
 */
interface ModuleInterface
{


    /**
     * Initializes the module.
     */
    public function init();

    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app);
}
