<?php

namespace engine\base\controllers;

use app\models\ResendVerificationEmailForm;
use app\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\ViewContextInterface;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use core\models\LoginForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\SignupForm;
use app\models\ContactForm;

/**
 * Site controller
 */
class BaseController extends Controller implements ViewContextInterface
{

//    public $layout = '@templates/basic/layouts/main';


    public function getViewPath()
    {
       return Yii::getAlias('@templates/basic/views/' . $this->module->id . DIRECTORY_SEPARATOR . Yii::$app->controller->id);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }
}
