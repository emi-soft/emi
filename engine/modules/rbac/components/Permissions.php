<?php
/**
 * Created by Maksim Skliarov.
 * Project: localhost
 * Company: Emi-Soft <emi.softdeveloper@gmail.com>
 * Date: 04/05/2019
 */

namespace engine\modules\rbac\components;

use Yii;

class Permissions
{


    public function getPermissionsByRole($role)
    {

        $manager = Yii::$app->authManager;
        $available = array_keys($manager->getPermissions());
        $availables = array_combine($available, $available);

        $assigned = array_keys($manager->getPermissionsByRole($role));
        $assigned = array_combine($assigned, $assigned);

        $availablesNew = [];
        foreach ($availables as $available) {
            $group = stristr($available, '/', true);
            $availablesNew[$group][$available] = $available;
        }

        $assignedNew = [];
        foreach ($assigned as $name) {
            $group = stristr($name, '/', true);
            $assignedNew[$group][$name] = $name;
            if(isset($availablesNew[$group][$name])){
                unset($availablesNew[$group][$name]);
            }
        }

        return [
            'available' => $availablesNew,
            'assigned' => $assignedNew
        ];
    }


}
