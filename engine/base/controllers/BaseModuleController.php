<?php

namespace engine\base\controllers;


use Yii;

/**
 * Core Module controller
 */
class BaseModuleController extends BaseController
{

    public function getViewPath()
    {
       return $this->module->getViewPath() . DIRECTORY_SEPARATOR . $this->id;
    }

}
