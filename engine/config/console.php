<?php
$params = array_merge(
    require __DIR__ . '/../../engine/config/params.php'
);
return [
    'params' => $params,
    'runtimePath'=> '@cache',
    'basePath'=> '/',
    'aliases' => [
        '@emi/managermodules' => '@engine/extension/manager-modules',
        '@emi/admin' => '@engine/extension/admin',
    ],
    'components' => [
//        'request' => [
//            'csrfParam' => '_csrf-frontend',
//            'cookieValidationKey' => 'MtWi0LImiu0NsVaUmrG3cyTJnvMYtfM_',
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
//        'errorHandler' => [
//            'errorAction' => 'main/error',
//        ],
//        'session' => [
//            'name' => 'app',
//        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=127.0.0.1;dbname=cms',
            'username' => 'root',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@core/mail',
            'useFileTransport' => true,
        ],
    ],

];
