<?php

namespace engine\base\components\module;

use Yii;

/**
 * This is the model class for table "{{%module_config}}".
 *
 * @property int $id
 * @property int $module_id
 * @property string $value
 *
 * @property ModuleModel $module
 */
class ModuleConfigModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%module_config}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'value'], 'required'],
            [['module_id'], 'integer'],
            [['value'], 'string'],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => ModuleModel::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module_id' => 'Module ID',
            'value' => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(ModuleModel::className(), ['id' => 'module_id']);
    }
}
