$('i.glyphicon-refresh-animate').hide();


function updateRoutes(r) {
    _opts.routes.available = r.available;
    _opts.routes.assigned = r.assigned;
    init('available');
    init('assigned');
}

$('#btn-new').click(function () {
    var $this = $(this);
    var route = $('#inp-route').val().trim();
    if (route != '') {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post($this.attr('href'), {route: route}, function (r) {
            $('#inp-route').val('').focus();
            updateRoutes(r);
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});


$('#btn-refresh').click(function () {
    var $icon = $(this).children('span.glyphicon');
    $icon.addClass('glyphicon-refresh-animate');
    $.post($(this).attr('href'), function (r) {
        updateRoutes(r);
    }).always(function () {
        $icon.removeClass('glyphicon-refresh-animate');
    });
    return false;
});

$('.search[data-target]').keyup(function () {
    init($(this).data('target'));
});



$('select[data-target]').dblclick(function () {
    var $this = $(this);
    var target = $this.data('target');
    var routes = $('select.list[data-target="' + target + '"]').val();

    var selected = $(':selected', this);
    console.log(selected.closest('optgroup').attr('label'));


    var href = '';
    if (target === 'available') {
        href = 'assign';
    } else if (target === 'assigned') {
        href = 'remove';
    }

    console.log(href);
    if (routes && routes.length) {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post(href, {routes: routes, group: _group}, function (r) {
            updateRoutes(r);
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});

$('.btn-assign').click(function () {
    var $this = $(this);
    var target = $this.data('target');
    var routes = $('select.list[data-target="' + target + '"]').val();

    if (routes && routes.length) {
        $this.children('i.glyphicon-refresh-animate').show();
        $.post($this.attr('href'), {routes: routes, group: _group}, function (r) {
            updateRoutes(r);
        }).always(function () {
            $this.children('i.glyphicon-refresh-animate').hide();
        });
    }
    return false;
});


function init(target) {
    var $list = $('select.list[data-target="' + target + '"]');
    $list.html('');
    var q = $('.search[data-target="' + target + '"]').val();

    createGroup($list, _opts.routes[target], q);
}

function createGroup(parent, array, q) {

    $.each(array, function (index, val) {
        let group = $('<optgroup   label="' + index + '" />');
        createItem(val, parent, group, q);
    });
}

function createItem(array, parent, group, q) {
    let added = false;

    if (typeof array === 'object') {

        $.each(array, function (index, val) {

            if (val.indexOf(q) >= 0) {
                $('<option />').text(val).appendTo(group);
                added = true;
            }
        });
    } else {

        if (array.indexOf(q) >= 0) {
            $('<option />').text(array).appendTo(group);
            added = true;
        }
    }


    if (added) {
        group.appendTo(parent);
    }
}

// initial
init('available');
//init('available');
init('assigned');
