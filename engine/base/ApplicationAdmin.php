<?php
/**
 * Created by PhpStorm.
 * User: clanfavorite
 * Date: 4/30/2019
 * Time: 10:46 AM
 */

namespace engine\base;


/**
 * Application is the base class for all web application classes.
 */
class ApplicationAdmin extends Application
{
    public $defaultRoute = 'main';
    public $controllerNamespace = 'engine\modules\admin\controllers';

    public function __construct($config = [])
    {
        $config['id'] = 'admin';
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    protected function bootstrap()
    {
              parent::bootstrap();
    }


}

