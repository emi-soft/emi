<?php

return [
    'components' => [

        'view' => [
            'theme' => [
                'basePath' => '@templates/basic',
                'baseUrl'   => '@web/templates/basic',
                'pathMap' => [

                    '@app/views' => '@templates/basic',
                    '@app/modules' => '@templates/basic/modules', // <-- !!!
                ],
            ],
        ],

        'request' => [
            'baseUrl' => '',
        ],
        'user' => [
            'identityClass' => 'engine\base\models\User',
            'loginUrl' => '/main/login',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity', 'httpOnly' => true],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'main/index',
//                '<controller:\w+>/<action:\w+>/' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/' => '<module>/<controller>/<action>',
            ],
        ],
    ]
];
