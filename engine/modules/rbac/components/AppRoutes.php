<?php

namespace engine\modules\rbac\components;


use Yii;

/**
 * @param array $routes
 */
class AppRoutes
{

    public $routes;

    public function __construct($module = false)
    {
        $this->getAppRoutes($module);
        $this->updateList();
    }

    /**
     * @return mixed
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    public function updateList()
    {
        $manager = Yii::$app->authManager;
        foreach ($this->routes as $route) {
            if (!$manager->getPermission($route)) {
                $permission = $manager->createPermission($route);
                $permission->description = $route;
                try {
                    $manager->add($permission);
                } catch (\Exception $e) {
                }
            }
        }
    }


    public function getRoutesByRole($role)
    {
        $manager = Yii::$app->authManager;
        $exists = [];
        foreach (array_keys($manager->getPermissionsByRole($role)) as $name) {
            preg_match('#\/(.*?)\/#', $name, $result);
            if (isset($exists[$result[0] . '*'])) {
                array_push($exists[$result[0] . '*'], $name);
            } else {
                $exists[$result[0] . '*'] = [];
                array_push($exists[$result[0] . '*'], $name);
            }
            unset($this->routes[$result[0] . '*'][$name]);
        }
        return [
            'available' => $this->routes,
            'assigned' => $exists
        ];
    }


    /**
     * @param null $module
     *
     * @return mixed
     */
    public function getAppRoutes($module = false)
    {
        if ($module === false) {
            $module = Yii::$app;
        } elseif (is_string($module)) {
            $module = Yii::$app->getModule($module);
        }
        if ($module) {
            $this->getRouteRecursive($module);
        }
        return $this->routes;
    }

    /**
     * @param $module
     * @param $result
     */
    protected function getRouteRecursive($module)
    {
        try {
            foreach ($module->getModules() as $id => $child) {
                if (($child = $module->getModule($id)) !== null) {
                    $this->getRouteRecursive($child);
                }
            }
            foreach ($module->controllerMap as $id => $type) {
                $this->getControllerActions($type, $id, $module, $this->routes);
            }
            if (!$module->getUniqueId()) {
                $all = ltrim('admin', '/');
            } else {
                $all = ltrim($module->getUniqueId(), '/');
            }
            $namespace = trim($module->controllerNamespace, '\\') . '\\';
            $this->getControllerFiles($module, $namespace, '', $this->routes);
//            $this->routes[$all] = $all;
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }

    /**
     * @param $module
     * @param $namespace
     * @param $prefix
     * @param $result
     */
    protected function getControllerFiles($module, $namespace, $prefix, &$result)
    {
        $path = Yii::getAlias('@' . str_replace('\\', '/', $namespace), false);
        try {
            if (!is_dir($path)) {
                return;
            }
            foreach (scandir($path) as $file) {
                if ($file == '.' || $file == '..') {
                    continue;
                }
                if (is_dir($path . '/' . $file) && preg_match('%^[a-z0-9_/]+$%i', $file . '/')) {
                    $this->getControllerFiles($module, $namespace . $file . '\\', $prefix . $file . '/', $result);
                } elseif (strcmp(substr($file, -14), 'Controller.php') === 0) {
                    $baseName = substr(basename($file), 0, -14);
                    $name = strtolower(preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $baseName));
                    $id = ltrim(str_replace(' ', '-', $name), '-');
                    $className = $namespace . $baseName . 'Controller';
                    if (strpos($className, '-') === false && class_exists($className) && is_subclass_of($className, 'yii\base\Controller')) {
                        $this->getControllerActions($className, $prefix . $id, $module, $result);
                    }
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }

    /**
     * @param $type
     * @param $id
     * @param $module
     * @param $result
     */
    protected function getControllerActions($type, $id, $module, &$result)
    {
        try {
            /* @var $controller \yii\base\Controller */
            $controller = Yii::createObject($type, [$id, $module]);
            $this->getActionRoutes($controller, $result);
            $all = "{$controller->uniqueId}/*";
            $result[$all] = $all;
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }

    /**
     * @param $controller
     * @param $result
     */
    protected function getActionRoutes($controller, &$result)
    {
        try {
            $prefix = $controller->uniqueId . '/';
            foreach ($controller->actions() as $id => $value) {
                $result[$prefix . $id] = $prefix . $id;
            }
            $class = new \ReflectionClass($controller);
            foreach ($class->getMethods() as $method) {
                $name = $method->getName();

                if ($method->isPublic() && !$method->isStatic() && strpos($name, 'action') === 0 && $name !== 'actions') {
                    $name = strtolower(preg_replace('/(?<![A-Z])[A-Z]/', ' \0', substr($name, 6)));

                    $id = $prefix . ltrim(str_replace(' ', '-', $name), '-');
                    $result[$id] = $id;
                }
            }
        } catch (\Exception $exc) {
            Yii::error($exc->getMessage(), __METHOD__);
        }
    }


}
