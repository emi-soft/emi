<?php

namespace engine\base\controllers;


use Yii;

/**
 * Core Module controller
 */
class EngineModuleController extends BaseController
{

    public function getViewPath()
    {
       return Yii::getAlias('@engine/modules/' . $this->module->id . '/views/' . Yii::$app->controller->id);
    }

}
