<?php

namespace engine\modules\rbac\controllers;

use engine\modules\admin\controllers\AdminController;
use engine\modules\rbac\components\Permissions;
use Yii;
use yii\base\Exception;
use yii\data\ArrayDataProvider;


/**
 * Default controller for the `rbac` module
 */
class RoleController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $data = [];
        foreach (\Yii::$app->authManager->getRoles() as $role) {
            $r['name'] = $role->name;
            $r['type'] = $role->type;
            $r['description'] = $role->description;
            $r['ruleName'] = $role->ruleName;
            $r['data'] = $role->data;
            $r['createdAt'] = $role->createdAt;
            $r['updatedAt'] = $role->updatedAt;
            $data[$role->name] = $r;
        }

        $provider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        return $this->render('index', [
            'provider' => $provider
        ]);
    }

    public function actionViewRole($name)
    {
        $Permissions = new Permissions();

        return $this->render('view-role', [
            'Permissions' => $Permissions->getPermissionsByRole($name),
            'groupName' => $name
        ]);
    }

    /**
     * Assign routes
     * @return array
     * @throws \Exception
     */
    public function actionAssign()
    {
        $routes = Yii::$app->getRequest()->post('routes', []);
        $groupName = Yii::$app->getRequest()->post('group');
        $manager = Yii::$app->authManager;
        $group = $manager->getRole($groupName);
        foreach ($routes as $route) {
            $permission = $manager->getPermission($route);
            try {
                $manager->addChild($group, $permission);

            } catch (Exception $e) {
            }

        }
        $Permissions = new Permissions();
        Yii::$app->getResponse()->format = 'json';
        return $Permissions->getPermissionsByRole($groupName);
    }

    /**
     * Assign routes
     * @return array
     * @throws \Exception
     */
    public function actionRemove()
    {
        $routes = Yii::$app->getRequest()->post('routes', []);
        $groupName = Yii::$app->getRequest()->post('group');

        $manager = Yii::$app->authManager;
        $group = $manager->getRole($groupName);
        foreach ($routes as $route) {
            $permission = $manager->getPermission($route);
            $manager->removeChild($group, $permission);
        }

        $Permissions = new Permissions();
        Yii::$app->getResponse()->format = 'json';
        return $Permissions->getPermissionsByRole($groupName);
    }
}
