<?php
/**
 * Created by PhpStorm.
 * User: clanfavorite
 * Date: 4/30/2019
 * Time: 10:46 AM
 */

namespace engine\base;

use engine\base\components\module\ModuleManager;
use engine\modules\rbac\components\AppRoutes;
use Yii;

/**
 * Application is the base class for all web application classes.
 */
class Application extends \yii\web\Application
{
    public function __construct($config = [])
    {
        if (!isset($config['bootstrap'])) {
            $config['bootstrap'] = [];
        }
        array_push($config['bootstrap'], 'log');
        array_push($config['bootstrap'], ModuleManager::className());
        $config['vendorPath'] = dirname(dirname(__DIR__)) . '/vendor';
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    protected function bootstrap()
    {

        Yii::setAlias('@bower', '@vendor/bower-asset');
        Yii::setAlias('@npm', '@vendor/npm-asset');
        parent::bootstrap();
        new AppRoutes();
    }


}

