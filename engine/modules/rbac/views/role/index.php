<?php

use yii\grid\GridView;
use yii\helpers\Url;

?>

<?= GridView::widget([
    'dataProvider' => $provider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'name',
        'type',
        'description',
        'ruleName',
        'data',
        'created_at',
        'updated_at',

        [
            'class' => 'yii\grid\ActionColumn',
            'urlCreator' => function ($action, $model, $key, $index) {
                return Url::to(['role/'.$action.'-role','name' => $key]);
            }
            ],
    ],
]); ?>
