<?php

namespace templates\basic;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AdminAsset extends AssetBundle
{
    public $sourcePath = '@vendor/almasaeed2010/adminlte/plugins';
    public $baseUrl = '@web';
    public $depends = [
        'dmstr\web\AdminLteAsset',
    ];

}
