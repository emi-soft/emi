<?php

use yii\db\Migration;

/**
 * Class m190504_102615_update_auth_item
 */
class m190504_102615_update_auth_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('auth_item', 'group', $this->string('255'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('auth_item', 'group');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190504_102615_update_auth_item cannot be reverted.\n";

        return false;
    }
    */
}
