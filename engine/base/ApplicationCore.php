<?php
/**
 * Created by PhpStorm.
 * User: clanfavorite
 * Date: 4/30/2019
 * Time: 10:46 AM
 */

namespace engine\base;

use engine\base\components\module\ModuleManager;
use Yii;

/**
 * Application is the base class for all web application classes.
 */
class ApplicationCore extends Application
{
    public $defaultRoute = 'main';
    public $controllerNamespace = 'engine\modules\core\controllers';

    public function __construct($config = [])
    {
        $config['id'] = 'core';
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    protected function bootstrap()
    {
        parent::bootstrap();
    }


}

