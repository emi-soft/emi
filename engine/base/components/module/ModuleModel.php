<?php

namespace engine\base\components\module;

use Yii;

/**
 * This is the model class for table "{{%module}}".
 *
 * @property int $id
 * @property string $url
 * @property string $path
 * @property string $include
 * @property string $type
 * @property int $status
 *
 * @property ModuleConfigModel[] $config
 */
class ModuleModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%module}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'path'], 'required'],
            [['status'], 'integer'],
            [['url', 'path'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 32],
            [['include'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'include' => 'Type',
            'type' => 'Type',
            'url' => 'Url',
            'path' => 'Path',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfig()
    {
        return $this->hasOne(ModuleConfigModel::className(), ['module_id' => 'id']);
    }
}
