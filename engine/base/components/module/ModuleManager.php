<?php
/**
 * Created by PhpStorm.
 * User: clanfavorite
 * Date: 4/30/2019
 * Time: 11:22 AM
 */

namespace engine\base\components\module;

use DirectoryIterator;
use ReflectionClass;
use Yii;
use yii\base\Component;
use yii\base\BootstrapInterface;
use yii\base\UnknownMethodException;
use yii\helpers\Json;

class ModuleManager extends Component implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $this->EnableAdminModule();
        $this->setEnabledModules();
    }

    public function registerModules()
    {
        $modules = $this->searchModules();

        foreach ($modules as $key => $module) {
            if ($module) {
                $installedModule = ModuleModel::find()->where(['path' => $module['path']])->one();

                if (!$installedModule) {
                    $newModule = new ModuleModel(['path' => $module['path'], 'url' => $key, 'type' => $module['type']]);
                    $newModule->save();
                    $newModuleConfig = new ModuleConfigModel(['value' => Json::encode($module['config']), 'module_id' => $newModule->id]);
                    $newModuleConfig->save();
                } else {
                    if ($installedModule->status) {
                        try {
                            Yii::$app->setModule($installedModule->url, Json::decode($installedModule->config->value));
                            Yii::$app->getModule($installedModule->url)->bootstrap(Yii::$app);
                        } catch (UnknownMethodException $e) {
                            Yii::error($e->getMessage());
                        }
                    }

                }
            }
        }
    }

    /**
     *
     */
    public function EnableAdminModule()
    {
        $adminModule = ModuleModel::find()
            ->where(['type' => 'admin'])
            ->one();
        try {
            Yii::$app->setModule($adminModule->url, Json::decode($adminModule->config->value));
            Yii::$app->getModule($adminModule->url)->bootstrap(Yii::$app);
        } catch (UnknownMethodException $e) {
            Yii::error($e->getMessage());
        }
    }

    public function setEnabledModules()
    {

        $installedModules = ModuleModel::find()
            ->where(['status' => 1])
            ->andFilterWhere(['!=', 'type' , 'admin'])
            ->all();
        $adminModule = Yii::$app->getModule('admin');
        foreach ($installedModules as $installedModule) {
            if ($installedModule->status) {
                try {
                    if ($installedModule->include == 'admin' || $installedModule->include == 'all') {
                        $adminModule->setModule($installedModule->url, Json::decode($installedModule->config->value));
                        $adminModule->getModule($installedModule->url)->bootstrap($adminModule);
                    }
                    if ($installedModule->include == 'core' || $installedModule->include == 'all') {
                        Yii::$app->setModule($installedModule->url, Json::decode($installedModule->config->value));
                        Yii::$app->getModule($installedModule->url)->bootstrap(Yii::$app);
                    }
                } catch (UnknownMethodException $e) {
                    Yii::error($e->getMessage());
                }

            }
        }
    }


    /**
     * @return mixed
     */
    public function searchModules()
    {
        $modulesDir = $this->dirModules();
        $modules = $this->getModulesOfDir($modulesDir);
        return $modules;
    }

    /**
     * @return array
     */
    public function dirModules()
    {
        $dir = new DirectoryIterator(Yii::getAlias('@modules'));
        $arDir = [];
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                array_push($arDir, $fileinfo->getFilename());
            }
        }
        return $arDir;
    }

    /**
     * @param $modulesDir
     * @return mixed
     */
    public function getModulesOfDir($modulesDir)
    {
        foreach ($modulesDir as $moduleDir) {
            $modules[$moduleDir] = false;
            $dir = new DirectoryIterator(Yii::getAlias('@modules/' . $moduleDir));
            foreach ($dir as $fileinfo) {
                if ($fileinfo->isFile() && !$fileinfo->isDot() && $fileinfo->getFilename() == 'config.json') {
                    $config = @file_get_contents($fileinfo->getRealPath());
                    $config = Json::decode($config);
                    try {
                        new ReflectionClass($config['config']['class']);
                        $config['path'] = '@modules/' . $moduleDir;
                        $modules[$config['name']] = $config;
                    } catch (\ReflectionException $e) {
                        Yii::error($e);
                    }
                }
            }
        }
        return $modules;
    }

}
